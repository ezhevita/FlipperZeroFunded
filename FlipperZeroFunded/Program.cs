﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telegram.Bot;

namespace FlipperZeroFunded {
	internal static class Program {
		private const long ChannelID = -1001447067578;

		private static readonly HttpClient Client = new HttpClient(new HttpClientHandler {
			AutomaticDecompression = DecompressionMethods.All
		}) {
			Timeout = TimeSpan.FromSeconds(10)
		};

		public static readonly CultureInfo DefaultCultureInfo = new CultureInfo("en-US", false);
		private static TelegramBotClient Bot;
		private static Dictionary<long, int> ChatsToAnnounce;
		private static uint NextGoal;

		private static void Main() {
			Client.DefaultRequestHeaders.TryAddWithoutValidation("User-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36");
			Bot = new TelegramBotClient(File.ReadAllText("token.txt"));
			ChatsToAnnounce = File.ReadAllLines("chats.txt").Select(line => line.Split(':')).ToDictionary(args => long.Parse(args[0]), args => int.Parse(args[1]));

			Timer timer = new Timer(async e => await PostInfo(),
				null,
				TimeSpan.FromSeconds(1),
				TimeSpan.FromMinutes(1)
			);

			Thread.CurrentThread.Join();
			timer.Dispose();
		}

		private static async Task PostInfo() {
			try {
				string text = await Client.GetStringAsync("https://www.kickstarter.com/projects/flipper-devices/flipper-zero-tamagochi-for-hackers/stats.json?v=1");
				Console.WriteLine(text);

				Project obj = JsonConvert.DeserializeObject<Project>(text);
				
				if (NextGoal == 0) {
					NextGoal = ((uint) obj.ProjectData.Pledged / 50000 + 1) * 50000;
				} else if (NextGoal <= obj.ProjectData.Pledged) {
					await Bot.SendTextMessageAsync(ChannelID, $"Собрано {NextGoal.ToString("C", DefaultCultureInfo)}!");
					NextGoal += 50_000;
				}

				foreach ((long chatID, int messageID) in ChatsToAnnounce) {
					await Bot.EditMessageTextAsync(chatID, messageID, $"Flipper Zero собрал {obj.ProjectData.Pledged.ToString("C", DefaultCultureInfo)}!\nУчастников: {obj.ProjectData.BackersCount}");
				}
			} catch {
				// ignored
			}
		}
	}

	public class Project {
		[JsonProperty(PropertyName = "project")]
		public InternalProject ProjectData;

		public class InternalProject {
			[JsonProperty(PropertyName = "backers_count")]
			public uint BackersCount;

			public decimal Pledged;

			[JsonProperty(PropertyName = "pledged")]
			private string PledgedText {
				get => Pledged.ToString(CultureInfo.InvariantCulture);
				set => Pledged = decimal.Parse(value, NumberStyles.Number, Program.DefaultCultureInfo);
			}
		}
	}
}
